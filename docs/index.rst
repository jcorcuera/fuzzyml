.. FuzzyML documentation master file, created by
   sphinx-quickstart on Thu Mar 23 17:23:03 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to FuzzyML's documentation!
===================================

Contents:

.. toctree::
   modules
   FuzzyML
   :maxdepth: 3

.. autosummary:: 
   :toctree: 
   FuzzyML
   
	
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

