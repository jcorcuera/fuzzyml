from setuptools import setup

setup(name="FuzzyML",
      version="0.1",
      description="Fuzzy Machine Learning library.",
      author="Barsacchi Marco",
      author_email="barsacchimarco@gmail.com",
      packages=["FuzzyML", "FuzzyML.datasets", "FuzzyML.discretization", "FuzzyML.fbdt",
                "FuzzyML.fmdt", "FuzzyML.test", "FuzzyML.utils"],
      install_requires=['numpy','numba','matplotlib'],
      include_package_data=True,
      long_description=open('README.md').read()
      )