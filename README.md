## Fuzzy Machine Learning Python Library (FuzzyML)

This project aims at implementing several fuzzy machine learning algorithms in python. The current version contains:

- Fuzzy Binary Decision Tree (FBDT)
- Fuzzy Multi-way Decision Tree (FMDT)
- Fuzzy Minimum Description Length Principle Discretizer (FMDLP)
- Crisp Minimum Description Length Principle Discretizer (MDLP)

The reference publication for the algorithms described here is:

A. Segatori, F. Marcelloni, W. Pedrycz, **"On Distributed Fuzzy Decision Trees for Big Data"**, *IEEE Transactions on Fuzzy Systems* 
DOI: [10.1109/TFUZZ.2016.2646746](https://doi.org/10.1109/TFUZZ.2016.2646746)

### Usage

#### Dependencies
FuzzyML requires:

- **Numpy** (basic numeric python library)
- **Numba** (Speed up computation generating machine code)
- **Matplotlib** (for plotting, used in pyTree plotting utils)
 
#### Installing the application
To install the application just clone the repository, `cd` into the folder and run
```bash
python setup.py install
```

#### Generate the documentation
The documentation for the project can be generated using sphinx.
First `cd` into the folder and launch:
```
sphinx-apidoc -f -o docs FuzzyML/ FuzzyML/test 
```
Then: 
```bash
cd docs
make html
```
The generated documentation will be located under `docs/build/html/`.

#### Examples

##### Plotting the discretization

- **numClasses**: Number of classes in the dataset.
- **continous**: True for each continous feature, False for each categorical feature.
- **minImpurity** (default = 0.02): Minimum fraction (w.r.t. the data size) of examples per node.
- **minGain** (default = 0.00001): Minimum entropy gain per spit.
- **threshold** (default=0): if threshold > 0 set the maximum number of fuzzy sets per features to threshold + 2.
            Fuzzy sets are obtained by backtracing splits point.
- **num_bins** (default=500): number of bins for the discretization step. A lower number of bins reduce the computational load.
- **ignore** (default=False): Combined with threshold != 0, ignore the MDLP for the first few splits.

```python

from __future__ import division, print_function

from time import time

import matplotlib.pyplot as plt
import numpy as np

from FuzzyML.datasets import load_iris
from FuzzyML.discretization import discretizer_fuzzy as df

# Load the dataset
Xtra, ytra = load_iris.load_iris()
# Specify the number of features
contv = [True] * 4
numFeat = len(contv)
rows = 2
cols = 2

t1 = time()

# Discretize a dataset
fdiscr = df.FuzzyMDLFilter(3, Xtra, ytra, continous=contv)
splits = fdiscr.run()

print("Time eval: %f" % (time() - t1))


plt.figure()

for subFeat in range(1, numFeat + 1):
    plt.subplot(rows, cols, subFeat)
    splits_points = splits[subFeat-1]
    if len(splits_points) != 0:
        fset1 = np.zeros_like(splits_points)
        fset1[::2] = 1.0
        fset2 = np.zeros_like(splits_points)
        fset2[1::2] = 1.0
        plt.plot(splits_points, fset1, 'k')
        plt.plot(splits_points, fset2, 'k')
    else:
        plt.plot()

    plt.title('Feature %d' % subFeat)
    if subFeat % rows == 1:
        plt.ylabel('fSets')
    if subFeat > (rows * (cols - 1)):
        plt.xlabel('Feature value')
    plt.grid()
plt.show()
```

![Discretization](examples/discretization/discr.png)


##### Generate a set of rules from a multi_way decision tree

FMDT parameters:
 
- **max_depth** (default = 15):maximum tree depth.
- **discr_minImpurity** (default = 0.02=): 
minimum impurity for a fuzzy set during discretization
- **discr_minGain** (default = 0.01):
minimum entropy gain during discretization
- **discr_threshold** (default = 0) 
if discr_threshold != 0  the discretization is stopped 
at n = discr_threshold + 2 fuzzy sets.
- **minGain** (default = 0.01)
            minimum entropy gain for a split during tree induction
- **minNumExample** (default = 2): 
minimum number of example for a node during tree induction
- **max_prop** (default = 1.0): 
min proportion of samples pertaining to the same class in the same node
for stop splitting.
- **prior_discretization** (default = True):
 define whether performing prior or node level discretization
 
```python
from __future__ import print_function

import matplotlib.pyplot as plt
import numpy as np

from FuzzyML.datasets import load_iris
from FuzzyML.fmdt import fmdt

# Dataset loading
X, y = load_iris.load_iris()
n, m = X.shape
np.random.seed(0)
index_tr = np.random.choice(range(n), size=int(n * 0.7), replace=False)
index_te = np.setdiff1d(np.arange(0, n, dtype=int), index_tr)

Xtr = X[index_tr, :]
ytr = y[index_tr]
Xte = X[index_te, :]
yte = y[index_te]

# Inducing the tree
fTree = fmdt.FMDT(minNumExamples=5, discr_threshold=0, max_depth=2, max_prop=1., minGain=0.0001,
                  priorDiscretization=True, verbose=True).fit(Xtr, ytr, [True] * Xtr.shape[1])
print(fTree.numLeaves())

print("Training accuracy: ",sum(fTree.predict(Xtr)==ytr)/float(len(ytr)))
print("Test accuracy: ",sum(fTree.predict(Xte)==yte)/float(len(yte)))

myArr =[]
fTree.tree._ruleMine([True]*Xtr.shape[1],ruleArray=myArr)
print()
for rule in myArr:
    print(rule)

plt.figure()
numFeat = 4
rows = 2
cols = 2
splits = fTree.cPoints

for subFeat in range(1, numFeat + 1):
    plt.subplot(rows, cols, subFeat)
    splits_points = splits[subFeat-1]
    if len(splits_points) != 0:
        fset1 = np.zeros_like(splits_points)
        fset1[::2] = 1.0
        fset2 = np.zeros_like(splits_points)
        fset2[1::2] = 1.0
        plt.plot(splits_points, fset1, 'k')
        plt.plot(splits_points, fset2, 'k')
    else:
        plt.plot()

    plt.title('Feature %d' % subFeat)
    if subFeat % rows == 1:
        plt.ylabel('fSets')
    if subFeat > (rows * (cols - 1)):
        plt.xlabel('Feature value')
    plt.grid()
plt.show()
```

Extracted rules:

```
if A3 is FS0 then [1.00, 0.00, 0.00]
if A3 is FS1 and A2 is FS0 then [1.00, 0.00, 0.00]
if A3 is FS1 and A2 is FS1 then [0.93, 0.07, 0.00]
if A3 is FS1 and A2 is FS2 then [0.00, 1.00, 0.00]
if A3 is FS1 and A2 is FS3 then [0.00, 1.00, 0.00]
if A3 is FS2 and A2 is FS1 then [0.00, 1.00, 0.00]
if A3 is FS2 and A2 is FS2 then [0.00, 0.99, 0.01]
if A3 is FS2 and A2 is FS3 then [0.00, 0.82, 0.18]
if A3 is FS2 and A2 is FS4 then [0.00, 0.07, 0.93]
if A3 is FS3 and A2 is FS1 then [0.00, 1.00, 0.00]
if A3 is FS3 and A2 is FS2 then [0.00, 0.82, 0.18]
if A3 is FS3 and A2 is FS3 then [0.00, 0.21, 0.79]
if A3 is FS3 and A2 is FS4 then [0.00, 0.01, 0.99]
if A3 is FS4 then [0.00, 0.00, 1.00]
```

##### Fuzzy binary decision tree
The FBDT parameters are:

- **max_depth** (default = 15):maximum tree depth.
- **discr_minImpurity** (default = 0.02=): 
minimum impurity for a fuzzy set during discretization
- **discr_minGain** (default = 0.01):
minimum entropy gain during discretization
- **discr_threshold** (default = 0) 
if discr_threshold != 0  the discretization is stopped 
at n = discr_threshold + 2 fuzzy sets.
- **minGain** (default = 0.01)
            minimum entropy gain for a split during tree induction
- **minNumExample** (default = 2): 
minimum number of example for a node during tree induction
- **max_prop** (default = 1.0): 
min proportion of samples pertaining to the same class in the same node
for stop splitting.
- **prior_discretization** (default = True):
 define whether performing prior or node level discretization
 
```python
from __future__ import print_function

import numpy as np

from FuzzyML.datasets import load_iris
from FuzzyML.fbdt import fbdt
from FuzzyML.utils import pyTree

# Dataset loading
X, y = load_iris.load_iris()
n, m = X.shape
np.random.seed(0)
index_tr = np.random.choice(range(n), size=int(n * 0.7), replace=False)
index_te = np.setdiff1d(np.arange(0, n, dtype=int), index_tr)

Xtr = X[index_tr, :]
ytr = y[index_tr]
Xte = X[index_te, :]
yte = y[index_te]

# Inducing the tree
myTree = fbdt.FBDT(max_depth=15, priorDiscretization=True, verbose=False,
                   ).fit(Xtr, ytr, [True, True, True, True])

print("Training on the IRIS dataset...")
print("Training accuracy: ",sum(myTree.predict(Xtr)==ytr)/float(len(ytr)))
print("Test accuracy: ",sum(myTree.predict(Xte)==yte)/float(len(yte)))
pyTree.evalPytTree(myTree, 3)
```
![Pythagorean representation](examples/tree/pythag_tree.png)

For the pythagorean representation description please refer to:

F. Beck, M. Burch, T. Munz, L. Di Silvestro and D. Weiskopf, **"Generalized Pythagoras Trees for visualizing hierarchies,"** *2014 International Conference on Information Visualization Theory and Applications (IVAPP), Lisbon, Portugal, 2014, pp. 17-28.*  
URL: [http://ieeexplore.ieee.org/document/7294394](http://ieeexplore.ieee.org/document/7294394)

### Roadmap
* Rewrite FuzzyMDLP API in a more sklearn oriented style
* Adding more algorithms
* ...