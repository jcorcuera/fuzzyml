from __future__ import print_function
from unittest import TestCase
from FuzzyML.datasets import load_iris
from FuzzyML.discretization.discretizer_crisp import CrispMDLFilter
import logging


class TestCrispMDLFilter(TestCase):

    def test_discretization(self):
        l = logging.getLogger('FuzzyMDLFilter')
        l.setLevel(logging.INFO)

        Xtra, ytra = load_iris.load_iris()
        contv = [True] * 4

        fdiscr = CrispMDLFilter(3, Xtra, ytra, continous=contv, minGain=0.005, minNumExamples=2)
        splits = fdiscr.run()

        self.assertIsNotNone(self,splits)
        print("Crisp Discretization test OK\n")
