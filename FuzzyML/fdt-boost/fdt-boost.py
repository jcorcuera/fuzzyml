#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 18 14:43:02 2016

@author: marcobarsacchi
"""
from __future__ import print_function
import numpy as np

from FuzzyML.discretization import discretizer_fuzzy as df


class FDTBoost():
    """
    Implements a SAMME multi-class AdaBoost algorithm according to [1]_.

    References
    ----------
    .. [1] Zhu, H. Zou, S. Rosset, T. Hastie, “Multi-class AdaBoost”, 2009.
    """

    def __init__(self, baseLearner, M=100, verbose=False, discr_minImpurity=0.02,
                 discr_minGain=0.000001, discr_threshold=0, minGain=0.01,
                 minNumExamples=2, priorDiscretization=True,
                 optional=None, seed=None,
                 inertia=0.0):

        """SAMME multi-class AdaBoost implementation.

        Parameters
        ----------
        baseLearner: class
            base learner to be used.
        M: int
            number of iterations that must be performed .
        verbose: Boolean
            activate or deactivate verbose output
        optional: dict
            dictionary of keywords arguments to be passed to be base learner
            upon instantiation.
        seed: int, optional, default = None
            optional seed for subset sampling.
        """
        # fittedModel will be an array of BaseLearners
        self.fittedModel = []
        self.baseLearner = baseLearner
        self.M = M
        self.alphaM = np.zeros(self.M)
        self.entropy = np.zeros(self.M)
        self.discr_minImpurity = discr_minImpurity
        self.discr_minGain = discr_minGain
        self.discr_threshold = discr_threshold
        self.minGain = minGain
        self.minNumExamples = minNumExamples
        self.priorDiscretization = priorDiscretization
        self.trained = False
        self.verbose = verbose
        self.optional = optional
        self.seed = seed
        assert (inertia >= 0 and inertia < 1), "Inertia parameter must be >= 0 and < 1"
        self.inertia = inertia

    def fit(self, tSet, labels, continous=None):

        self.fittedModel = []
        epsilon = 10 ** -3
        self.tSet = tSet
        self.currentSet = tSet[:]
        self.labels = labels
        self.currentLabels = labels[:]
        self.continous = continous
        # Number of training samples
        self.N = self.currentSet.shape[0]
        # Number of Classes
        self.K = len(set(labels))
        # Initial weights
        self.cPoints = None
        self.oldErr = np.zeros(self.N)

        self.weights = np.ones(self.N) / self.N
        if self.priorDiscretization:

            # The discretization phase will be unique.
            discr = df.FuzzyMDLFilter(self.K, tSet, labels, list(self.continous),
                                      minGain=self.discr_minGain,
                                      minImpurity=self.discr_minImpurity,
                                      threshold=self.discr_threshold)
            self.cPoints = np.array(discr.run())

            if self.optional is None:
                self.optional = {'priorDiscretization': True}
            else:
                self.optional['priorDiscretization'] = True
        if self.verbose:
            print("Discretization ended...")

        for m in range(self.M):
            if self.verbose and (not m % 5):
                print("Training model %d of %d" % (m + 1, self.M))
            if self.optional is None:
                # Modified here
                self.fittedModel.append(self.baseLearner().fit(self.currentSet, self.currentLabels, continous, self.K))
            else:
                # Modified here
                self.fittedModel.append(
                    self.baseLearner(**self.optional).fit(self.currentSet, self.currentLabels, continous, self.K,
                                                          self.cPoints))

            errM = self.__computeError(m)
            # SAMME
            self.alphaM[m] = np.log((1 - errM + epsilon) / (errM + epsilon)) + np.log(self.K - 1)

            self.entropy[m] = - np.sum(self.weights * np.log(self.weights))
            self.__recomputeWeights(m)
            # Modified here
            self.__updateSet(m)

        self.trained = True
        return self

    def __recomputeWeights(self, m):

        self.err = (self.labels != self.fittedModel[m].predict(self.tSet))
        dW = self.weights * np.exp(self.alphaM[m] * (self.err)) + self.inertia * self.oldErr
        self.weights = self.weights + dW

        self.oldErr = dW[:]


        self.weights = self.weights / np.sum(self.weights)


    def _margin_predict(self, X, fuzzy=True):
        if not self.trained:
            raise Exception('Model must be trained before prediction.')

        numTest = X.shape[0]
        sum_m = np.sum(self.alphaM)
        if fuzzy:
            pvect = np.zeros((self.K, numTest))
            for k in range(numTest):

                p4 = np.zeros(self.K)
                datum = X[k, :]
                for m in range(self.M):
                    predicted = self.fittedModel[m].predictRF(datum)
                    p4 += predicted * self.alphaM[m]

                pvect[:, k] = p4
            return pvect / sum_m
        else:
            pvect = np.zeros((self.K, numTest))
            for k in range(numTest):

                p4 = np.zeros(self.K)
                datum = X[k, :].reshape(1, -1)
                for m in range(self.M):
                    predicted = self.fittedModel[m].predict(datum)
                    p4[predicted] += self.alphaM[m]

                pvect[:, k] = p4
            return pvect / sum_m

    def predict(self, X, until=None, fuzzy=True):
        """Predict class or regression value for X.

        For a classification model, the predicted class for each sample in X is
        returned. For a regression model, the predicted value based on X is
        returned.

        Parameters
        ----------
        X : array-like  of shape = [n_samples, n_features]
            The input samples.

        Returns
        -------
        y : array of shape = [n_samples] or [n_samples, n_outputs]
            The predicted classes, or the predict values.
        """
        if not self.trained:
            raise Exception('Model must be trained before prediction.')

        numTest = X.shape[0]
        if fuzzy:
            if until:
                pvect = np.zeros((numTest, self.M))
                for k in range(numTest):

                    p4 = np.zeros((self.K, self.M))
                    datum = X[k, :].reshape(1, -1)
                    for m in range(self.M):
                        predicted = self.fittedModel[m].predict(datum)
                        p4[:, m] += predicted * self.alphaM[m]
                    p4 = np.cumsum(p4, axis=1)
                    pvect[k, :] = np.argmax(p4, axis=0)
                return pvect

            else:
                pvect = np.zeros(numTest)
                for k in range(numTest):

                    p4 = np.zeros(self.K)
                    datum = X[k, :]
                    for m in range(self.M):
                        predicted = self.fittedModel[m].predictRF(datum)
                        p4 += predicted * self.alphaM[m]

                    pvect[k] = np.argmax(p4)
                return pvect
        else:
            if until:
                pvect = np.zeros((numTest, self.M))
                for k in range(numTest):

                    p4 = np.zeros((self.K, self.M))
                    datum = X[k, :].reshape(1, -1)
                    for m in range(self.M):
                        predicted = self.fittedModel[m].predict(datum)
                        p4[predicted, m] += self.alphaM[m]
                    p4 = np.cumsum(p4, axis=1)
                    pvect[k, :] = np.argmax(p4, axis=0)
                return pvect
            else:
                pvect = np.zeros(numTest)
                for k in range(numTest):

                    p4 = np.zeros(self.K)
                    datum = X[k, :].reshape(1, -1)
                    for m in range(self.M):
                        predicted = self.fittedModel[m].predict(datum)
                        p4[predicted] += self.alphaM[m]

                    pvect[k] = np.argmax(p4)
                return pvect

    def __computeError(self, m):
        weightSum = np.sum(self.weights)
        eerW = 0
        eerW = np.sum((self.labels != self.fittedModel[m].predict(self.tSet)) * self.weights)
        return np.nan_to_num(eerW / weightSum)

    def __updateSet(self, m):
        normWeights = self.weights / np.sum(self.weights)
        if self.seed:
            np.random.seed(self.seed + m)
        rW = np.random.choice(np.arange(self.N), self.N, p=normWeights)
        self.currentSet = self.tSet[rW, :]
        self.currentLabels = self.labels[rW]
