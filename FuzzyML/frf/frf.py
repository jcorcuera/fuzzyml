#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Created on Tue Nov 29 09:44:26 2016

@author: marcobarsacchi
"""
from __future__ import print_function
import functools
import numpy as np
from FuzzyML.fbdt import fbdt as fbdt
from FuzzyML.discretization import discretizer_fuzzy as df


class FRF():
    """
    Implements a Fuzzy Random Forest algorithm according to 
    """
    
    def __init__(self, M=100, features='sqrt',oobEval=False,
                 max_depth=15, discr_minImpurity=0.02,
                 discr_minGain=0.000001, discr_threshold=0, minGain=0.01,
                 minNumExamples=2, max_prop = 1.0, priorDiscretization=True,
                 seed = None, optional={}, verbose=False):
        
        """RF implementation.
        
        Parameters
        ----------
        M: int
            number of trees to be grown.
        verbose: Boolean
            activate or deactive verbose output
        optional: dict
            dictionary of keywords arguments to be passed to be base learner 
            upon instantiation.
        """
        # fittedModel will be a list of BaseLearners
        self.fittedModel = []
        self.M = M
        self.alphaM = np.zeros(self.M)
        self.max_depth = max_depth
        self.discr_minImpurity = discr_minImpurity
        self.discr_minGain = discr_minGain
        self.discr_threshold = discr_threshold
        self.minGain = minGain
        self.minNumExamples = minNumExamples
        self.max_prop = max_prop 
        self.verbose = verbose
        self.optional = optional
        self.features = features
        self.oobEval = oobEval
        self.priorDiscretization = priorDiscretization
        self.trained = False   
        self.seed = seed
        assert features in ['all', 'sqrt', 'log2'], "features must be in [all', 'sqrt', 'log2']"
    
    def fit(self, tSet, labels, continous=None, cPoints=None):
        
        self.fittedModel = []
        self.tSet = tSet
        self.labels = labels
        self.continous=continous
        # Number of training samples
        self.N = self.tSet.shape[0]
        # Number of Classes
        self.K = len(set(labels))
        # Initial weights
        self.weights = np.ones(self.M)
        if cPoints:
            self.cPoints = cPoints
        else:
            if self.priorDiscretization:
                # The discretization phase will be unique.
                discr = df.FuzzyMDLFilter(self.K, tSet, labels, list(self.continous), 
                                      minGain=self.discr_minGain,
                                      minImpurity=self.discr_minImpurity,
                                      threshold = self.discr_threshold)
                self.cPoints = np.array(discr.run())
            else:
                self.cPoints = None

        for m in range(self.M):
            if self.verbose and m%5 == 0:
                print("Training tree %d of %d" %(m+1, self.M))
            
            # Bagging
            if self.seed is not None:
                np.random.seed(self.seed)
                self.seed = self.seed +1
                
            bagged_index = np.random.choice(range(self.N),size=self.N)
            oobIndex = np.setdiff1d(np.arange(0, self.N,dtype=int), bagged_index)
            
            self.currentSet = self.tSet[bagged_index,:]
            self.currentLabels = self.labels[bagged_index]
            self.fittedModel.append(fbdt.FBDT(features=self.features,
                                                    priorDiscretization=True,seed=self.seed,
                                                    **self.optional).fit(self.currentSet,
                                                    self.currentLabels, continous, self.K,
                                                    cPoints=self.cPoints))


            # OOB computation
            if self.oobEval:
                Xte = self.tSet[oobIndex,:] 
                yte = self.labels[oobIndex]
                self.weights[m] = 1. - np.sum(self.fittedModel[-1].predict(Xte)!=yte)/float(len(yte))
  
        self.trained = True
        return self
        

            
    def predict(self,X):
        """Predict class or regression value for X.

        For a classification model, the predicted class for each sample in X is
        returned. For a regression model, the predicted value based on X is
        returned.

        Parameters
        ----------
        X : array-like  of shape = [n_samples, n_features]
            The input samples. 

        Returns
        -------
        y : array of shape = [n_samples] or [n_samples, n_outputs]
            The predicted classes, or the predict values.
        """
        
        if not self.trained:
            raise Exception('Model must be trained before prediction.')

        numTest = X.shape[0]

        pvect = np.zeros(numTest)
        
        for k in range(numTest):
            if self.oobEval:
                pvect[k] =  np.argmax(functools.reduce(lambda x,y : x+y, 
                [self.fittedModel[j].predictRF(X[k,:])*w for j,w in zip(range(self.M),self.weights)]))

            else:
                pvect[k] =  np.argmax(functools.reduce(lambda x,y : x+y, 
                [self.fittedModel[j].predictRF(X[k,:]) for j in range(self.M)]))
        return pvect
